<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Edit Status</title>
</head>
<body>
<h2>Edit Status</h2>
<p>You name is ${user.name}</p>
<form:form>
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone Number</th>
        </tr>
        <c:forEach var="application" items="${user.applications}">
            <tr>
                <td><c:out value="${application.firstName}"/></td>
                <td><c:out value="${application.lastName}"/></td>
                <td><c:out value="${application.address}"/></td>
                <td><c:out value="${application.phoneNumber}"/></td>
            </tr>
        </c:forEach>
    </table>
    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}"/>
    <input type="submit" name="_eventId_onExit" value="Exit"/>
    <input type="submit" name="_eventId_onAdd" value="Add"/>
</form:form>
</body>
</html>
