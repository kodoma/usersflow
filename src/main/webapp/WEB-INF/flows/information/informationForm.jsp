<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org"
      xmlns:c="http://java.sun.com/jsp/jstl/core">
<html>

<head>
    <title>Information Form</title>
</head>

<body>
<h2>Registration Form</h2>

<form:form>
    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}"/>
    <input type="text" name="name"/><br/>
    <input type="text" name="password"/><br/>
    <input type="submit" name="_eventId_onEnter" value="Ok"/>
</form:form>

</body>

</html>