/**
Создание новой заявки
 */
USE users_db;
DELIMITER /
DROP PROCEDURE IF EXISTS addNewApplication /
CREATE PROCEDURE addNewApplication(IN entityData VARCHAR(200), IN status VARCHAR(30),
IN phoneNumber INT, IN userId INT)
  BEGIN
    INSERT INTO users_db.applications
    VALUES (NULL, entityData, status, phoneNumber, userId);
  END;
/
DELIMITER ;

CALL addNewApplication("", "kkk", 111, 2);
