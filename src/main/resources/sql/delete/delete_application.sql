/**
Удаление заявки
 */
DELIMITER /
DROP PROCEDURE IF EXISTS removeApplication /
CREATE PROCEDURE removeApplication(IN applicationId INT, IN userId INT)
  BEGIN
    DELETE FROM users_db.applications
    WHERE application_id = applicationId AND user_id = userId;
  END;
/
DELIMITER ;

CALL removeApplication(5, 3);