/**
Создание таблиц
 */
START TRANSACTION;

DROP TABLE IF EXISTS users_db.users;

/**
Создание таблицы users
 */
CREATE TABLE users_db.users (
  user_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  password VARCHAR(400),
  PRIMARY KEY (user_id));

DROP TABLE IF EXISTS users_db.applications;

/**
Создание таблицы applications
 */
CREATE TABLE users_db.applications (
  application_id INT NOT NULL AUTO_INCREMENT,
  entity_data VARCHAR(400),
  status VARCHAR(30),
  phone_number INT(20),
  user_id INT,
  PRIMARY KEY (application_id),
  FOREIGN KEY (user_id) REFERENCES users_db.users (user_id));

COMMIT;
# ROLLBACK