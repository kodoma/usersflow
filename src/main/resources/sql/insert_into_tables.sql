/**
Заполнение таблиц тестовыми данными
 */
START TRANSACTION;

/**
Заполнение таблицы users
 */
INSERT INTO users_db.users VALUES (NULL, 'Kira', 123);
INSERT INTO users_db.users VALUES (NULL, 'Lila', 457);
INSERT INTO users_db.users VALUES (NULL, 'Ronald', 357);

DELETE FROM applications;

/**
Заполнение таблицы applications
 */
INSERT INTO users_db.applications VALUES (NULL, '{
  "applicationId": 34,
  "firstName": "Дмитрий",
  "lastName": "Соколов",
  "address": {
    "country": "Россия",
    "city": "Нижний Новгород"
  },
  "phone": "8(915)951-07-34",
  "attributes": [35, 12, 84]
}', 'Одобрено', 2415475, 1
);

INSERT INTO users_db.applications VALUES (NULL, '{
  "applicationId": 35,
  "firstName": "Максим",
  "lastName": "Воронков",
  "address": {
    "country": "Россия",
    "city": "Нижний Новгород"
  },
  "phone": "8(900)774-56-75",
  "attributes": [12, 15, 41]
}', 'Одобрено', 2416543, 1
);

INSERT INTO users_db.applications VALUES (NULL, '{
  "applicationId": 34,
  "firstName": "Евгений",
  "lastName": "Батиков",
  "address": {
    "country": "Россия",
    "city": "Москва"
  },
  "phone": "8(908)417-54-19",
  "attributes": [14, 41, 11]
}', 'Одобрено', 2411574, 1
);

COMMIT;
# ROLLBACK