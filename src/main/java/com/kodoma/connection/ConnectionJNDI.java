package com.kodoma.connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionJNDI {
    private static Connection connection;
    private static ConnectionJNDI instance;
    private static DataSource dataSource;

    private ConnectionJNDI() {
    }

    public static synchronized Connection getConnection() {
        if (instance == null) {
            try {
                instance = new ConnectionJNDI();
                Context context = new InitialContext();
                instance.dataSource = (DataSource) context.lookup("java:comp/env/jdbc/users_db");
                connection = dataSource.getConnection();
            } catch (NamingException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
