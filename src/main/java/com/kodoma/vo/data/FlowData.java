package com.kodoma.vo.data;

import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import java.util.HashMap;
import java.util.Map;

public class FlowData {
    private static FlowData flowData;
    private static Map<String, Object> attributesMap = new HashMap<>();

    private FlowData() {
    }

    public RequestContext getContext() {
        return RequestContextHolder.getRequestContext();
    }

    public static FlowData getFlowData() {
        if (flowData == null) {
            flowData = new FlowData();
        }
        Map<String, Object> contextMap = flowData.getContext().getFlowScope().asMap();
        for (Map.Entry<String, Object> entry : contextMap.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
            attributesMap.put(entry.getKey(), entry.getValue());
        }
        return flowData;
    }

    public Map<String, Object> getAttributesMap() {
        return attributesMap;
    }

    public void setAttributesMap(Map<String, Object> map) {
        attributesMap = map;
    }
}
