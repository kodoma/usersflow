package com.kodoma.utils;

import com.kodoma.vo.data.FlowData;
import org.springframework.webflow.core.collection.AttributeMap;
import org.springframework.webflow.core.collection.LocalAttributeMap;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import java.util.HashMap;
import java.util.Map;

public class Transition extends Event {
    private FlowData flowData;

    public Transition(Object source, String id) {
        super(source, id);
    }

    public Transition(Object source, String id, AttributeMap<Object> attributes) {
        super(source, id, attributes);
    }

    public static Transition go(String idTransition, FlowData flowData) {
        RequestContext context = RequestContextHolder.getRequestContext();
        System.out.println("EVENT: " + context.getCurrentEvent());
        Object obj = context.getCurrentEvent().getSource();
        System.out.println("EVENT SOURCE: " + obj);

        MutableAttributeMap<Object> mutableAttributeMap = context.getAttributes();
        Map<String, Object> mapA = mutableAttributeMap.asMap();

        for (Map.Entry<String, Object> entry : mapA.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        Map<String, Object> map = flowData.getAttributesMap();
        //Object source = context.getCurrentEvent().getSource();
        Object source = flowData.getContext();
        AttributeMap attributeMap = new LocalAttributeMap(map);

        return new Transition(source, idTransition, attributeMap);
    }

    public FlowData getFlowData() {
        return flowData;
    }

    public void setFlowData(FlowData flowData) {
        this.flowData = flowData;
    }
}

