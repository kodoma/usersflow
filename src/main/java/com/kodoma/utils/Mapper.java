package com.kodoma.utils;

import com.kodoma.vo.User;
import com.kodoma.vo.data.FlowData;

import java.util.HashMap;
import java.util.Map;

public class Mapper {
    public static User mapToUser(FlowData flowData) {
        Map<String, Object> attributes = flowData.getAttributesMap();
        return (User)attributes.get("user");
    }

    public static Map<String, Object> UserToMap(User user) {
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        return map;
    }
}
