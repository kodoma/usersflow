package com.kodoma.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.webflow.core.collection.AttributeMap;
import org.springframework.webflow.core.collection.LocalAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import java.util.HashMap;
import java.util.Map;

public class TransitionOld extends Event {
    public TransitionOld(Object source, String id) {
        super(source, id);
    }

    public TransitionOld(Object source, String id, AttributeMap<Object> attributes) {
        super(source, id, attributes);
    }

    public static TransitionOld go(String idTransition) {
        SecurityContextHolder.getContext().getAuthentication();

        RequestContext context = RequestContextHolder.getRequestContext();

        Map<String, Object> contextMap = context.getFlowScope().asMap();
        Map<String, Object> map = new HashMap<>();

        for (Map.Entry<String, Object> entry : contextMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }

        Object source = context.getCurrentEvent().getSource();
        AttributeMap attributeMap = new LocalAttributeMap(map);
        return new TransitionOld(source, idTransition, attributeMap);
    }
}
