package com.kodoma.utils;

public class Commands {
    public static final String GET_USER = "SELECT * FROM users_db.users WHERE name=? AND password=?;";
    public static final String GET_ALL_APPLICATIONS = "SELECT entity_data FROM applications WHERE user_id = ?;";
    public static final String ADD_APPLICATION = "CALL addNewApplication(?, ?, ?, ?);";
}
