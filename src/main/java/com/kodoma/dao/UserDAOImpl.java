package com.kodoma.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kodoma.exeptions.ApplicationAddException;
import com.kodoma.vo.Application;
import com.kodoma.vo.User;
import com.kodoma.connection.ConnectionPool;
import com.kodoma.exeptions.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.kodoma.utils.Commands.ADD_APPLICATION;
import static com.kodoma.utils.Commands.GET_ALL_APPLICATIONS;
import static com.kodoma.utils.Commands.GET_USER;

public class UserDAOImpl implements UserDAO<User> {
    private static final Connection connection = ConnectionPool.getConnection();
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDAOImpl.class);

    public static void main(String[] args) {
        try {
            new UserDAOImpl().getAllApplications(1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
    }

    public User getUser(String name, String password) throws UserNotFoundException {
        LOGGER.info("getUser() method");
        User user = null;
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(GET_USER);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            ResultSet result = preparedStatement.executeQuery();
            String resultName = "";
            String resultPassword = "";
            int resultId = 0;

            while (result.next()) {
                resultId = result.getInt("user_id");
                resultName = result.getString("name");
                resultPassword = result.getString("password");
            }
            user = new User(resultId, resultName, resultPassword);
        } catch (SQLException e) {
            throw new UserNotFoundException("Пользователь не найден.");
        }
        return user;
    }

    public List<Application> getAllApplications(int userId) throws IOException, UserNotFoundException {
        List<Application> applications = new ArrayList<>();
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(GET_ALL_APPLICATIONS);
            preparedStatement.setInt(1, userId);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                ObjectMapper mapper = new ObjectMapper();
                Application application = mapper.readValue(result.getString("entity_data"), Application.class);
                applications.add(application);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserNotFoundException("Пользователь не найден.");
        }
        return applications;
    }

    @Override
    public void addApplication(int userId) throws ApplicationAddException {
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(ADD_APPLICATION);
            preparedStatement.setString(1, "123");
            preparedStatement.setString(2, "Compliance");
            preparedStatement.setInt(3, 111);
            preparedStatement.setInt(4, 3);
            preparedStatement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
