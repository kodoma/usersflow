package com.kodoma.handlers.orchestratioin;

import com.kodoma.vo.data.FlowData;
import org.springframework.stereotype.Component;

@Component
public class MainOrchestration {
    private FlowData flowData;

    public FlowData getFlowData() {
        return FlowData.getFlowData();
    }
}
