package com.kodoma.handlers;

import com.kodoma.exeptions.ApplicationAddException;
import com.kodoma.exeptions.UserNotFoundException;
import com.kodoma.handlers.orchestratioin.MainOrchestration;
import com.kodoma.utils.Mapper;
import com.kodoma.vo.User;
import com.kodoma.servises.UserService;
import com.kodoma.vo.data.FlowData;
import com.kodoma.utils.Transition;
import com.kodoma.vo.data.FlowData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.webflow.action.EventFactorySupport;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class UserMainHandler {

    @Autowired
    private UserService<User> service;

    @Autowired
    private MainOrchestration orchestration;

    public Transition getAllApplications() {
        FlowData flowData = orchestration.getFlowData();
        User user = Mapper.mapToUser(flowData);

        List applications = null;
        try {
            applications = service.getAllApplications(user.getId());
        } catch (IOException | UserNotFoundException e) {
            e.printStackTrace();
            return Transition.go("cancel", flowData);
        }
        user.setApplications(applications);
        flowData.setAttributesMap(Mapper.UserToMap(user));
        return Transition.go("editStatus", flowData);
    }

    public Transition addApplication() {
        FlowData flowData = orchestration.getFlowData();
        User user = Mapper.mapToUser(flowData);
        try {
            service.addApplication(user.getId());
        } catch (ApplicationAddException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping
    public void getSession(HttpSession session) {
        Enumeration<String> enumeration = session.getAttributeNames();

        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }
    }

    //Фабрика
    private EventFactorySupport eventFactorySupport = new EventFactorySupport();
    //Обрати внимание на возвращаемый тип

    public Event select(User user) {
        // идём по транзакции selectOne
        List applications = null;
        try {
            applications = service.getAllApplications(user.getId());
        } catch (IOException | UserNotFoundException e) {
            return eventFactorySupport.event(this,"cancel");
        }
        user.setApplications(applications);
        return eventFactorySupport.event(user,"editStatus");
    }

    public void getFlowData() {
        FlowData.getFlowData();
    }
}
