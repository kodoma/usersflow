package com.kodoma.exeptions;

public class ApplicationAddException extends Exception {
    public ApplicationAddException(String message) {
        super(message);
    }
}
