package com.kodoma.servises;

import com.kodoma.exeptions.ApplicationAddException;
import com.kodoma.vo.Application;
import com.kodoma.vo.User;
import com.kodoma.dao.UserDAO;
import com.kodoma.exeptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;


@Component
public class UserServiceImpl implements UserService<User> {

    @Autowired
    private UserDAO<User> dao;

    public User getUser(String name, String password) throws UserNotFoundException {
        User user = dao.getUser(name, password);
        user.setFound(true);
        if (user.getName().equals("") || user.getPassword().equals("")) {
            user.setFound(false);
            throw new UserNotFoundException("Пользователь не найден.");
        }
        return user;
    }

    public List<?> getAllApplications(int userId) throws IOException, UserNotFoundException {
        return dao.getAllApplications(userId);
    }

    @Override
    public void addApplication(int userId) throws ApplicationAddException {
        dao.addApplication(userId);
    }
}
