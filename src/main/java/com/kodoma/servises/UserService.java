package com.kodoma.servises;

import com.kodoma.exeptions.ApplicationAddException;
import com.kodoma.exeptions.UserNotFoundException;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public interface UserService<T extends Serializable> {
    T getUser(String name, String password) throws UserNotFoundException;
    List<?> getAllApplications(int userId) throws IOException, UserNotFoundException;
    void addApplication(int userId) throws ApplicationAddException;
}
